package ru.endlesscode.eventslogger.bukkit

import org.bukkit.plugin.java.JavaPlugin
import ru.endlesscode.eventslogger.common.LogRule


class EventsLoggerPlugin : JavaPlugin() {

    private var isEventsLoggerEnabled: Boolean = false
    private var isPacketsLoggerEnabled: Boolean = false

    init {
        loadConfig()

        if (isEventsLoggerEnabled) {
            enableEventsLogger()
        }
    }

    override fun onEnable() {
        if (isPacketsLoggerEnabled) {
            enablePacketsLogger()
        }
    }

    private fun loadConfig() {
        config.options().copyDefaults(true)
        saveConfig()

        isEventsLoggerEnabled = config.getBoolean("EventsLogger.enabled", false)
        isPacketsLoggerEnabled = config.getBoolean("PacketsLogger.enabled", false)
    }

    private fun enableEventsLogger() {
        val showHierarchy = config.getBoolean("EventsLogger.hierarchy", true)
        val eventsLogger = EventsLogger(server.consoleSender, loadEventsLogRules(), showHierarchy)
        eventsLogger.inject(this)
    }

    private fun enablePacketsLogger() {
        if (!server.pluginManager.isPluginEnabled("ProtocolLib")) {
            logger.warning("ProtocolLib not found, packets logger will be disabled!")
            return
        }

        val packetsLogger = PacketsLogger(server.consoleSender, loadPacketsLogRules())
        packetsLogger.inject(this)
    }

    private fun loadEventsLogRules(): Map<String, LogRule> {
        return loadLogRules(config.getStringList("EventsLogger.log"))
    }

    private fun loadPacketsLogRules(): Map<String, LogRule> {
        return loadLogRules(config.getStringList("PacketsLogger.log"))
    }

    private fun loadLogRules(rules: List<String>): Map<String, LogRule> {
        return rules.map {
            val rule = LogRule.fromString(it)
            rule.name to rule
        }.toMap()
    }
}
