package ru.endlesscode.eventslogger.bukkit.util


internal fun String.firstUpperCase(): String {
    return if (isNotEmpty()) substring(0, 1).toUpperCase() + substring(1).toLowerCase() else this
}
