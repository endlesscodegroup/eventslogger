# EventsLogger

![Events Logging Example](https://gitlab.com/endlesscodegroup/eventslogger/raw/develop/images/event_log_example.png)

It contains two tools:
- EventLogger - tool to log events
- PacketsLogger - tool to log packets (needs ProtocolLib)

This plugin helpful for plugins developers. With it, they can track when and what events occurred and what packets sent.

### Config example
```yaml
# Events logger it is the tool that helps developers to debug events
EventsLogger:
  enabled: false
  # What we need to log
  # Here you can use super classes to configure event groups
  log:
  - Event                               # Log all events
  - PlayerStatisticIncrementEvent:100   # Skip this event 100 times
  - PlayerMoveEvent:20                  
  - -ChunkEvent                         # Don't log all events that extends ChunkEvent
  - -BlockEvent                          
  - -VehicleEvent                       
  - -EntityAirChangeEvent               # Don't log the event

# Packets logger it is the tool that helps developers to debug packets
PacketsLogger:
  enabled: false
  # What we need to log
  # Format: <protocol>[.<source>[.<name>]]
  # Minecraft protocol: https://wiki.vg/Protocol
  # ProtocolLib packet types: https://github.com/aadnk/ProtocolLib/blob/master/modules/API/src/main/java/com/comphenix/protocol/PacketType.java
  log:
  - Handshake                           # Log all protocols
  - Status
  - Login
  - Play
  - Play.Client.POSITION:20             # Skip this packet 20 times
  - -Play.Client.LOOK                   # Don't want these frequent events
  - -Play.Server.MAP_CHUNK
  - -Play.Server.UPDATE_TIME
  - -Play.Server.ENTITY_HEAD_ROTATION
  - -Play.Server.ENTITY_VELOCITY
  - -Play.Server.ENTITY_TELEPORT
  - -Play.Server.ENTITY_METADATA
  - -Play.Server.ENTITY_LOOK
  - -Play.Server.ENTITY_STATUS
  - -Play.Server.REL_ENTITY_MOVE
  - -Play.Server.REL_ENTITY_MOVE_LOOK
```
